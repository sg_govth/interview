resource "aws_s3_bucket" "sg_bucket" {
    bucket          = "${var.bucket_name}"
    force_destory   = "${var.force_destory}"
    region          = "${var.region}"
    tags            = "SG_Bucket"
}